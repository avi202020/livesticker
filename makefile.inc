# Configurations for makefile.
# Edit this file to specify the paths and versions of required tools and directories.

export os := $(shell uname)

ifeq ($(os),Darwin)
	include env.mac
endif

ifeq ($(os),Linux)
	include env.linux
endif

export AWSBucketName=livesticker

export ThirdPartyJarDir=$(Transient)/ThirdPartyJars

# Configurations particular to why this build is being run:
export maxerrs = 5

# Versions produced by this build:
export VERSION = 0.1
export BUILD_TAG = 0

# Output directories.
export build_dir = $(Transient)
export jar_dir = $(Transient)/jars

# User executing this script.
export UserHome := $(HOME)

# Locations of dev tools:
export MVN_REPO := $(HOME)/.m2/repository

CUCUMBER_CLASSPATH := $(MVN_REPO)/info/cukes/cucumber-java/1.2.5/cucumber-java-1.2.5.jar
CUCUMBER_CLASSPATH := $(CUCUMBER_CLASSPATH):$(MVN_REPO)/info/cukes/cucumber-core/1.2.5/cucumber-core-1.2.5.jar
CUCUMBER_CLASSPATH := $(CUCUMBER_CLASSPATH):$(MVN_REPO)/info/cukes/cucumber-jvm-deps/1.0.5/cucumber-jvm-deps-1.0.5.jar
CUCUMBER_CLASSPATH := $(CUCUMBER_CLASSPATH):$(MVN_REPO)/info/cukes/gherkin/2.12.2/gherkin-2.12.2.jar
export CUCUMBER_CLASSPATH

AWS_CLASSPATH := $(MVN_REPO)/com/amazonaws/aws-java-sdk-s3/1.11.327/aws-java-sdk-s3-1.11.327.jar
AWS_CLASSPATH := $(AWS_CLASSPATH):$(MVN_REPO)/com/amazonaws/aws-java-sdk/1.11.327/aws-java-sdk-1.11.327.jar
