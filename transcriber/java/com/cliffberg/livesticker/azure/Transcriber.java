package com.cliffberg.livesticker.azure;

/*
 Ref: https://docs.microsoft.com/en-us/azure/cognitive-services/speech/getstarted/getstartedrest?tabs=Powershell
 Ref: https://docs.microsoft.com/en-us/azure/cognitive-services/speech/home
 */

import java.io.StringReader;
import java.net.URI;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.IOException;
import javax.net.ssl.SSLContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.Variant;
import javax.ws.rs.WebApplicationException;
import javax.json.JsonReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonArray;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.CommonProperties;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;

public class Transcriber {
	
	/**
	 * Transcribe the media file at the specified URL.
	 */
	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.out.println(usage());
			System.exit(1);
		}
		
		System.out.println((new Transcriber()).transcribe(new File(args[0]),
			args[1]));
	}
	
	public static String usage() {
		return "args:\n" +
			"\turl - wav file to transcribe\n" +
			"\taws_access_key_id\n" +
			"\taws_secret_access_key";
	}
	
	private static String RecognitionMode = "interactive";
	private static String LanguageTag = "en-US";
	private static String OutputFormat = "simple";
	
	private static String BaseURL =
		"https://speech.platform.bing.com/speech/recognition/" +
		RecognitionMode +
		"/cognitiveservices/v1?language=" +
		LanguageTag +
		"&format=" +
		OutputFormat
		;
	
	private static String OcpApimSubscriptionKeyHeaderName = "Ocp-Apim-Subscription-Key";
	private static String ContentTypeHeaderName = "Content-type";
	private static String ContentType = "audio/wav; codec=audio/pcm; samplerate=16000";
	private static String TransferEncodingHeaderName = "Transfer-Encoding";
	private static String AcceptHeaderName = "Accept";
	private static String Accept = "application/json;text/xml";
	private static String ExpectHeaderName = "Expect";
	private static String Expect = "100-continue";
	
	/**
	 * Invoke Microsoft Speech service to transcribe the audio file.
	 */
	public String transcribe(File audioFile, String subscriptionKey) throws Exception {
		
		Map<String, String> otherHeaders = new HashMap<String, String>();
		otherHeaders.put(OcpApimSubscriptionKeyHeaderName, subscriptionKey);
		otherHeaders.put(AcceptHeaderName, Accept);
		otherHeaders.put(ExpectHeaderName, Expect);
		InputStream stream = new FileInputStream(audioFile);
		
		Response response = makePostRequest(BaseURL, ContentType, otherHeaders, stream);
		
		// Parse response.
		/* Sample response:
		{
			"RecognitionStatus":"Success",
			"DisplayText":"Once Upon a midnight dreary.",
			"Offset":5400000,
			"Duration":12500000
		}
		 */
		
		String responseBody = response.readEntity(String.class);
		System.out.println("Response body-----");
		System.out.println(responseBody);
		System.out.println("-----");
		JsonReader reader = Json.createReader(new StringReader(responseBody));
		JsonObject json = reader.readObject();
		String status = json.getString("RecognitionStatus");
		if (! status.equals("Success")) throw new Exception(status);
		String transcribedText = json.getString("DisplayText");
		
		return transcribedText;
	}
	
	protected Response makeGetRequest(String uri, String[]... params) throws Exception {
		
		Invocation.Builder builder = createHttpRequestBuilder(uri, params);
		Response response = builder.buildGet().invoke();
		return response;
	}
	
	/**
	 * Perform a POST request. Query parameters are provided as a Map, or may
	 * be null. body may be null.
	 * IMPORTANT: It is essential to call close() on the Response object.
	 */
	protected Response makePostRequest(String uri, String contentType,
		Map<String, String> otherHeaders,
		InputStream bodyStream, String[]... params) throws Exception {
		
		Invocation.Builder builder = createHttpRequestBuilder(uri, params);
		Response response = null;
		
		for (String headerName: otherHeaders.keySet()) {
			builder.header(headerName, otherHeaders.get(headerName));
		}
		
		if (bodyStream == null) {
			response = builder.buildPost(null).invoke();
		} else {
			
			StreamingOutput jerseyStream = new StreamingOutput() {
				
				public void write(OutputStream outputStream) throws IOException,
					WebApplicationException {
					
					// Write to Jersey OutputStream chunk by chunk here
					System.err.println("Writing data...");
					int nwritten = 0;
					try {
						byte[] buffer = new byte[10000];
						for (;;) {
							int n = bodyStream.read(buffer);
							outputStream.write(buffer, 0, n);
							nwritten += n;
							if (n < 10000) return;
						}
					} finally {
						System.err.println(nwritten + " bytes written");
					}
				}
			};
			
			// ContentType = "audio/wav; codec=audio/pcm; samplerate=16000";
			String[] contentParts = contentType.split(";");
			String[] typeSubtype = contentParts[0].split("/");
			Map<String, String> contentTypeParams = new HashMap<String, String>();
			for (int i = 1; i < contentParts.length; i++) {
				String[] paramParts = contentParts[i].split("=");
				contentTypeParams.put(paramParts[0].trim(), paramParts[1].trim());
			}
				
			MediaType mediaType = new MediaType(
				typeSubtype[0].trim(), typeSubtype[1].trim(), contentTypeParams);
			
			System.out.println("MediaType=" + mediaType.toString());
			Entity<StreamingOutput> entity = Entity.entity(jerseyStream, mediaType);
			Invocation invocation = builder.buildPost(entity);
			response = invocation.invoke();
		}
		
		return response;
	}
	
	protected Invocation.Builder createHttpRequestBuilder(String path, String[]... params)
		throws Exception {
		
		URI uri = new URI(path);
		
		Registry<ConnectionSocketFactory> socketFactoryRegistry =
			RegistryBuilder.<ConnectionSocketFactory>create()
				.register("https", new SSLConnectionSocketFactory(SSLContext.getDefault()))
				.build();
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.connectorProvider(new ApacheConnectorProvider());
		clientConfig.property(CommonProperties.FEATURE_AUTO_DISCOVERY_DISABLE, true);

		BasicHttpClientConnectionManager connManager =
			new BasicHttpClientConnectionManager(socketFactoryRegistry);
		//clientConfig.register(ResponseStatusExceptionFilter.class);
		//clientConfig.register(JsonClientFilter.class);
		//clientConfig.register(JacksonJsonProvider.class);
		clientConfig.property(ApacheClientProperties.CONNECTION_MANAGER, connManager);
	
		ClientBuilder clientBuilder = ClientBuilder.newBuilder().withConfig(clientConfig);
		
		Client client = clientBuilder.build();
		
		WebTarget target = client.target(path);
		
		for (String[] keyValuePair : params) {
			if (keyValuePair.length != 2) throw new RuntimeException(
				"Expected a key, value pair; found: " + keyValuePair.toString());
			target = target.queryParam(keyValuePair[0], keyValuePair[1]);
		}
		
		Invocation.Builder invocationBuilder =
			target.request(MediaType.TEXT_PLAIN_TYPE);
		
		return invocationBuilder;
	}

	/**
	 * Must satisfy ^[0-9a-zA-Z._-]+
	 */
	protected String createUniqueJobName() {
		return String.valueOf((new Date()).getTime());
	}
}
